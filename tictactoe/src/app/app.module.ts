//Dependencias Angular
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './routes/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule  } from "@angular/fire/compat";
import { AngularFirestoreModule } from "@angular/fire/compat/firestore";

//Componentes Renderizacion
import { AppComponent } from './app.component';
import { InicioComponent } from './game/inicio/inicio.component';
import { TableroComponent } from './game/tablero/tablero.component';
import { MarkComponent } from './game/mark/mark.component';
//Servicio apra consumo
import { FirebaseService } from "./game/services/firebase.service";
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    TableroComponent,
    MarkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule
  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
