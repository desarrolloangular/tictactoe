/**
 * @interface mark (interfaz para referenciar marcacion de las casillas del juego)
 * @author Juan David Guerrero Vargas
 */
export interface mark{
    position ?: number,
    status ?: boolean,
    ind_x ?:boolean,
    ind_o ?:boolean
}

/**
 * @interface jugador (interfaz para control de datos sobre el jugador)
 * @author Juan David Guerrero Vargas
 */
export interface jugador{
    name ?: string,
    id ?: number
}
/**
 * @interface Game (interfaz para control de datos del juego en session)
 * @author Juan David Guerrero Vargas
 */
export interface Game{
    jugadores ?: jugador[],
    hash ?: string,
    id_game ?: string,
    mark_ini ?: string,
    status ?: boolean,
    mark_game ?:string,
    tablero ?:mark[]
}