import { Component, Input  } from '@angular/core';
import { mark } from "../interfaces/ganeral.interface";

@Component({
  selector: 'app-mark',
  templateUrl: './mark.component.html',
  styleUrls: ['./mark.component.scss']
})
export class MarkComponent {
  //variable que recibe los datos con los que se imprimira la marca en la casilla
  @Input('content') marca : mark ;
  constructor() {
    this.marca = {}
   }



}
