import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Game, jugador, mark } from "../interfaces/ganeral.interface";
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class FirebaseService {

    constructor(private http: HttpClient) { }
    public base_url :string = "https://tic-tac-toe-121a3-default-rtdb.firebaseio.com/";

    /**
     * 
     * @param table (Recibe la tabla o el conjunto de  elemento a ingresar o eliminar dentro de Firebase)
     * @param game Recibe un objeto el cual tiene el estatus actual del juego y el estado del tablero
     * @returns Observable(Game) Con el estado del juego, listo para subscribirse y obtener el retorno del lado de la logica
     */
    generatePost(table:string, game:Game):Observable<Game>{
        return this.http.post(`${this.base_url}${table}`, game );
    }

    /**
     * 
     * @param table (Tabla a consultar dentro de FireBase)
     * @returns Arreglo de Observables de Tipo Game
     */
    getRegistros(table:string):Observable<Game[]>{
        return this.http.get<Game[]>(`${this.base_url}${table}`);
    }

}
