import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { mark, Game, jugador } from "../interfaces/ganeral.interface";
import { FirebaseService } from "../services/firebase.service";

@Component({
    selector: 'app-tablero',
    templateUrl: './tablero.component.html',
    styleUrls: ['./tablero.component.scss']
})

export class TableroComponent {
    public games:Game[] = [];
    //variable al Cual contendra lso mensajes de rstpuesta sobre el estado del juego
    public mensaje: string = "";
    //Variable para control de partida
    public game: Game = {
        id_game: '1',
        mark_ini: 'X',
        hash: 'abcdfghtmlp',
        jugadores: [
            { id: 1, name: 'Player 1' },
            { id: 2, name: 'Player 2' }
        ],
        status: false
    };
    //Variable para matriz que contiene las marcas dentro del tablero
    public campo: mark[] = [
        { status: false }, { status: false }, { status: false }, //[1],[2],[3]
        { status: false }, { status: false }, { status: false }, //[4],[5],[6]
        { status: false }, { status: false }, { status: false }, //[7],[8],[9]


    ];
    
    /**
     * @method constructor (Metodo que inicializa el juegador que iniciara la partida)
     */
    constructor(private fireBaseService:FirebaseService,
        ) {
            
        /*mETODO PARA OBTENER LOS DATOS DE FIREBASE
        this.fireBaseService.getRegistros('game.json').subscribe(
            (respond:Game[]) =>{
                //console.log(respond)
                console.log("*******")
                console.log(typeof respond)
                console.log("*******")
                this.games = respond;
               // console.log(this.games);
                console.log("*******")
                
            } 
            
        );*/
        let marca = localStorage.getItem('marca');
        if (marca != "" && marca != null) {
            if(marca == 'X'){
                
                this.game.mark_game = 'O';
                localStorage.setItem('marca', 'O');
            }else if(marca == 'O'){
                
                this.game.mark_game = 'X';
                localStorage.setItem('marca', 'X');
            }
        } else {
            localStorage.setItem('marca', 'X');
            this.game.mark_game = 'X';
        }
    }
    /**
     * 
     * @param marck (Objeto que representa la marca en el tablero)
     * @param indice (Indice de la podicion en que se imprimio el objeto de  Marca)
     * @author Juan David Guerrero Vargas
     */
    jugar(marck: mark, indice: number) {
        this.game.tablero = this.campo;

      
   

        this.campo[indice] = {
            position: indice,

        };
        //Validamos si quedan pocisiones por jugar
        let estado = this.validaTablero();

        if (estado && this.game.status == false) {
            let letra_jugada = String(this.game.mark_game);
            
            if (this.game.mark_game === 'X' && (this.campo[indice].status == false || this.campo[indice].status == undefined)) {

                this.campo[indice].ind_x = true;
               
                this.game.mark_game = 'O';

            } else if (this.game.mark_game === 'O' && (this.campo[indice].status == false || this.campo[indice].status == undefined)) {
                
                this.campo[indice].ind_o = true;
                this.game.mark_game = 'X';

            }
            this.campo[indice].status = true;
            //Validamos si desoues de realizar la jugaa se ha logrado la victoria
            let victoria = this.validaVictoria(letra_jugada);
            /*Metodo que registra la marcacion de los campos en la base de datos 
            this.fireBaseService.generatePost('game.json', this.game).subscribe(
                respond => console.log(respond),
                error => console.log("Error generado" + error)  
            );*/
        } else {
            this.mensaje = 'Juego Finalizado';
            this.game.status = true;
        }


    }
    /**
     * 
     * @returns true si todavia quedan campos disponibles por jugar , False si ya se realizaron todas las jugadas posibles y no se encontro un ganador
     * @author Juan David Guerrero Vargas
     */
    validaTablero(): boolean {
        let ind_true = 0;
        let ind_false = 0;
        for (let i = 0; i < this.campo.length; i++) {

            let campoI = this.campo[i];


            if (campoI.status == false || campoI.status == undefined ) {
                ind_false++;

            }

            if (campoI.status == true) {
                ind_true++;
            }
        }
    
        if (ind_false >= 2 && ind_true <= 8) {
            return true;
        } else {
            this.game.status = true;
            this.mensaje = 'Juego Empatado';

            return false;
        }
    }
    /**
     * 
     * @param type (PArametro tipo string que recibe el jugador el cual realizo la movida valores recibidos X - O )
     * @returns true Si algun Jugador alcanzo la victoria, false si el tablero todavia no tiene ganador o empate
     * * @author Juan David Guerrero Vargas
     */
    validaVictoria(type: string): boolean {
        //Arreglo que dentro de cada posicion contiene todas las posibilidades con las que se puede ganar dependiendo del tipo de jugador
        let condicion = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        for (let i = 0; i < condicion.length; i++) {
            let valid_act = condicion[i];
            //Dependiendo el tipo de jugador se valdia la marcacion de la casillas 
            //Con esto se determina cual es el Ganador dependiendo de quein realio la jugada
            switch (type) {
                case 'X':
                    if (this.campo[valid_act[0]].ind_x == true &&
                        this.campo[valid_act[1]].ind_x == true &&
                        this.campo[valid_act[2]].ind_x == true) {
                        this.mensaje = "El jugador X ha ganado";
                        this.game.status = true;
                        return true;
                    }
                    break;
                case 'O':
                    if (this.campo[valid_act[0]].ind_o == true &&
                        this.campo[valid_act[1]].ind_o == true &&
                        this.campo[valid_act[2]].ind_o == true) {
                        this.mensaje = "El jugador O ha ganado";
                        this.game.status = true;
                        return true;
                    }
                    break;
            }


        }
        //Validamos que todavia queden campos por jugar 
        let cont = 0;
        for (let i = 0; i < this.campo.length; i++) {
            if (this.campo[i].status == true) {
                cont++;
            }
        }
        //Si todos los campos fueron jugados se declara el empate del Juego
        if (cont == 9) {
            this.mensaje = "El juego ha terminado en empate";
            this.game.status = true;
            return true;
        }
        return false;
    }

    /**
     * @method resetGame (funcion que permite reiniciar el tablero apra un nuevo juego)
     * @author Juan David Guerrero Vargas
     */
    resetGame() {
        for(let i = 0; i < this.campo.length; i++){
            this.campo[i].ind_o = false;
            this.campo[i].ind_x = false;
            this.campo[i].status = false;
            this.mensaje = "";
            this.game.status = false;
         }

    }

}
