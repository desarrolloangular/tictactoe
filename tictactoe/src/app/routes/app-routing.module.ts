import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from "../game/inicio/inicio.component";
import { TableroComponent } from "../game/tablero/tablero.component";

const routes: Routes = [
  {
    path:'inicio',
    component:InicioComponent

  }, 
  {
    path:'nueva_partida',
    component:TableroComponent

  },
  {
    path:'',
    pathMatch:'full',
    redirectTo:'inicio'
  },
  {
    path:'**',
    pathMatch:'full',
    redirectTo:'inicio'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
